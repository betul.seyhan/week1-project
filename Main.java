package CS102;

public class Helloworld {
    //herkes için,you don't need object to initilize or run it,not return anything
	public static void main(String args[]) { //main method
		Circle circle=new Circle();
		printSummary(0,circle);
		circle.setX(-2.06);
		circle.setY(-0.23);
		circle.setR(1.33);
		printSummary(1,circle);
		circle.setX(0.0);
		circle.setY(5.0);
		circle.setR(1.88);
		printSummary(2,circle);
		circle.translate(5.0,0.0);
		printSummary(3,circle);
		System.out.println("Area " +circle.getArea());
		circle.scale(0.5);
		printSummary(4,circle);
		circle.translate(-1.,-1);
		circle.setR(3.1);
		printSummary(5,circle);
		
	}
	public static void printSummary(int state,Circle circle) {
		System.out.print("-------------------------------------");
		System.out.println("state:"+state+" ,X: "+circle.getX());
		System.out.println("state:"+state+" ,Y: "+circle.getY());
		System.out.println("state:"+state+" ,R: "+circle.getR());
	}
}
